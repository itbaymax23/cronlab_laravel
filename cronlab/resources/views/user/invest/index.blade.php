@extends('layouts.dashboard')
@section('title', 'Pick the best invest plan for you')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card" style="background-image: url('{{asset('img/4.jpg')}}'); background-size: 100% 100%">
                <div class="card-title"></div>

                <div class="card-content">
                    <br>
                    @if($invests)
                        @foreach($invests as $invest)
                        
                            <div class="col-md-6 col-md-offset-3">
                                <div class="card card-pricing card-raised">
                                    <div class="card-content">
                                        <h6 class="category">{{$invest->name}}</h6>
                                        <div class="icon icon-info">
                                            <i class="material-icons">highlight</i>
                                        </div>
                                        <h5 class="card-title text-warning">
                                            <b> {{config('app.currency_symbol')}} {{$invest->minimum + 00}}</b>

                                            <!-- <b> {{config('app.currency_symbol')}} {{$invest->minimum + 00}} - {{config('app.currency_symbol')}} {{$invest->maximum + 00}}</b> -->
                                        </h5>
                                        <br>
                                        <p class="card-description">
                                            You will get Return <span class="text-success"> <b> {{$invest->percentage + 0}}% </b></span> monthly on every investment. Interest are calculated from Monday to friday. This is a Monthly Plan.

                                        </p>
                                        <button class="btn btn-raised btn-round btn-info" data-toggle="modal" data-target="#investModal{{$invest->id}}">
                                            Buy a share
                                        </button>
                                    </div>
                                </div>
                            </div>
                        

                            <!-- small modal -->
                            <div class="col-md-6 col-md-offset-3">
                                <div class="modal fade" id="investModal{{$invest->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-small ">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">
                                                <h5>Enter Invest Amount  Below</h5>
                                            </div>
                                            <form action="{{route('userInvestment.submit')}}" method="post">
                                                {{ csrf_field() }}
                                                @if(count($errors) > 0)
                                                    <div class="alert alert-danger alert-with-icon" data-notify="container">
                                                        <i class="material-icons" data-notify="icon">notifications</i>
                                                        <span data-notify="message">
                                                            @foreach($errors->all() as $error)
                                                                <li><strong> {{$error}} </strong></li>
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                @endif

                                                <div class="modal-body">
                                                        <div class="form-group label-floating">
                                                            <label  class="control-label" for="amount">Investment Amount</label>
                                                            <input id="amount" name="amount" type="number" class="form-control" min="1" value="1">
                                                        </div>

                                                    <input type="hidden" name="plan_id" value="{{$invest->id}}">

                                                </div>

                                            <div class="modal-footer text-center">
                                                <button type="button" class="btn btn-simple" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-success btn-sm">Preview Invest</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                            <!--    end small modal -->

                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>




@endsection