@extends('layouts.dashboard')
@section('title', 'My Referral & Link')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="rose">
                    <i class="material-icons">contacts</i>
                </div>
                <br>
                <h4 class="card-title">My Referral</h4>
                <div class="card-content">
                    @if($user->admin == 0)
                    <span style="font-size: 20px;">My Referral Code: 
                        @for($code_num=0;$code_num<$num;$code_num++)
                        <code class="text-center">
                            {{ $referred_code[$code_num] }};&nbsp;
                        </code>
                        @endfor
                    </span>
                    <br>
                    @if($parent_id != 0)
                    <br>
                    <div class="row">
                        <div class="col-md-5">
                            <span style="font-size: 20px;">Name of My Sponsor:&nbsp;&nbsp;</span>
                            <span style="font-size: 18px; color: #c7254e;">{{$parent_info->name}}</span>
                        </div>
                        <div class="col-md-6">
                            <span style="font-size: 20px;">Email Address of My Sponsor:&nbsp;</span>
                            <span style="font-size: 18px; color: #c7254e;">{{$parent_info->email}}</span>
                        </div>
                    </div>
                    @endif
                    <div class="table-responsive">
                        
                        @if(count($reflinks) > 0 && $num != 0)
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="text-center">SN</th>
                                <th class="text-center">Photo</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Referred Code</th>
                                <th class="text-center">Total Earn</th>
                                <th class="text-center">Today Earn</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Join Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $id=0;@endphp
                                @for($i=0;$i<$num;$i++)

                                    @foreach($reflinks as $reflink)
                                        @if($referred_code[$i] == $reflink->link)
                                        @php $id++;@endphp
                                        <tr>
                                            <td class="text-center">{{ $id }}</td>
                                            <td class="text-center" width="10%" >
                                                <img src="{{$reflink->user->profile->avatar}}" class="img-circle" alt="No Photo"  >
                                            </td>
                                            <td class="text-center">{{$reflink->user->name}}</td>
                                            <td class="text-center">{{$reflink->link}}</td>
                                            <td class="text-center">$ {{$reflink->total + 0}}</td>
                                            <td class="text-center">$ {{$reflink->today +0}}</td>
                                            <td class="text-center">

                                                @if($reflink->user->active == 0)
                                                    Not Active
                                                @else
                                                    Active
                                                @endif

                                            </td>
                                            <td class="text-center">{{ date("j/ n/ Y", strtotime($reflink->user->created_at)) }}</td>
                                        </tr>
                                        @endif
                                    @endforeach

                                @endfor
                            @if($id == 0)
                            <h1> There is no Referral User.</h1>
                            @endif
                            </tbody>
                        </table>
                        @else
                            <h1> There is no Refer You have made.</h1>

                        @endif
                        
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection