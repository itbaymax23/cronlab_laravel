@extends('layouts.dashboard')
@section('title', 'My Account Dashboard')
@section('content')

   <div class="row">
       @if(count($notify) > 0)
       <div class="col-md-12">
           <div class="card-content">
               <div class="alert alert-danger alert-with-icon" data-notify="container">
                   <i class="material-icons" data-notify="icon">notifications</i>
                   <span data-notify="message">
                       You have {{count($notify)}} un-read message
                   </span>
               </div>
           </div>
           <br><br><br>
       </div>
       @endif
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="purple">
                    <i class="material-icons">monetization_on</i>
                </div>
                <div class="card-content">
                    <p class="category">{{config('app.currency_code')}}</p>
                    <h5 class="card-title">{{config('app.currency_symbol')}} {{$user->profile->main_balance + 0}}</h5>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons text-danger">account_circle</i>
                        Account Balance
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="rose">
                    <i class="material-icons">attach_money</i>
                </div>
                <div class="card-content">
                    <p class="category">{{config('app.currency_code')}}</p>
                    <h5 class="card-title">{{config('app.currency_symbol')}} {{$user->profile->referral_balance + 0}}</h5>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons text-rose">compare_arrows</i> Referral Balance
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">done_all</i>
                </div>
                <div class="card-content">
                    <p class="category">{{config('app.currency_code')}}</p>
                    <h5 class="card-title">{{config('app.currency_symbol')}} {{$user->profile->deposit_balance + 0}}</h5>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons text-success">eject</i> Deposited Balance
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">https</i>
                </div>
                <div class="card-content">
                    <p class="category">{{config('app.currency_code')}}</p>
                    <h5 class="card-title">{{config('app.currency_symbol')}} {{$withdraw + 0}}</h5>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons text-info">hourglass_empty</i>Total Withdraw
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>


@endsection

