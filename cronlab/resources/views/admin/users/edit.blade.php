@extends('layouts.admin')

@section('title', 'Edit Member Profile')

@section('content')


    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="purple">
                    <i class="material-icons">perm_identity</i>
                </div>
                <div class="card-content">
                    <h4 class="card-title">Edit Member Profile -
                        <small class="category">Complete Member profile</small>
                    </h4>

                    <form action="{{route('admin.user.update',['id'=>$user->id])}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if(count($errors) > 0)
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">notifications</i>
                            <span data-notify="message">

                                    @foreach($errors->all() as $error)
                                        <li><strong> {{$error}} </strong></li>
                                    @endforeach

                            </span>
                        </div>
                        @endif

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="{{asset($user->profile->avatar)}}" alt="...">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div>
                                        <span class="btn btn-rose btn-round btn-file">
                                            <span class="fileinput-new">Select image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="avatar" />
                                        </span>
                                        <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <select class="selectpicker" name="admin" data-style="btn btn-warning btn-round" title="Single Select" data-size="7">
                                        <option value="0"
                                                @if($user->admin == 0)
                                                selected
                                                @endif
                                        >Not Admin</option>
                                        <option value="1"
                                                @if($user->admin == 1)
                                                selected
                                                @endif
                                        >Already Admin</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <select class="selectpicker" name="active" data-style="btn btn-warning btn-round" title="Single Select" data-size="7">
                                        <option value="0"
                                                @if($user->active == 0)
                                                selected
                                                @endif
                                        >Not Active</option>
                                        <option value="1"
                                                @if($user->active == 1)
                                                selected
                                                @endif
                                        >Already Active</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <select class="selectpicker" name="type" data-style="btn btn-warning btn-round" title="Single Select" data-size="7">
                                        <option value="Person"
                                                @if($user->profile->type == 'Person')
                                                selected
                                                @endif
                                        >Person</option>
                                        <option value="Company"
                                                @if($user->profile->type == 'Company')
                                                selected
                                                @endif
                                        >Company</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="name">Full Name</label>
                                    <input id="name" name="name" type="text" value="{{$user->name}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="email">Email Address</label>
                                    <input id="email" name="email" value="{{$user->email}}" type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="occupation">Occupation</label>
                                    <input id="occupation" name="occupation" type="text" value="{{$user->profile->occupation}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="country">Member Country</label>
                                    <input id="country" name="country" type="text" value="{{$user->profile->country}}" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="address">Address Line 1</label>
                                    <input id="address" name="address" value="{{$user->profile->address}}"  type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="address2">Address Line 2</label>
                                    <input id="address2" name="address2" value="{{$user->profile->address2}}" type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="city">City</label>
                                    <input id="city" name="city" type="text" value="{{$user->profile->city}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="state">State</label>
                                    <input id="state" name="state" type="text" value="{{$user->profile->state}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="postcode">Postal Code</label>
                                    <input id="postcode" name="postcode" type="text" value="{{$user->profile->postcode}}" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="mobile">Mobile Number</label>
                                    <input id="mobile" name="mobile" type="text" value="{{$user->profile->mobile}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="telephone">Telephone Number</label>
                                    <input id="telephone" name="telephone" type="text" value="{{$user->profile->telephone}}" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="main_balance">Main Balance</label>
                                    <input id="main_balance" name="main_balance" value="{{$user->profile->main_balance}}" type="number" class="form-control">

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="deposit_balance">Deposit Balance</label>
                                    <input id="deposit_balance" name="deposit_balance" value="{{$user->profile->deposit_balance}}"  type="number" class="form-control">

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="referral_balance">Referral Balance</label>
                                    <input id="referral_balance" name="referral_balance" value="{{$user->profile->referral_balance}}"  type="number" class="form-control">

                                </div>
                            </div>
                        </div>
                        <br>
                        <h5>Bank Account1</h5>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="account_holder">Account holder</label>
                                    <input id="account_holder" name="account_holder" type="text" value="{{$user->bank->account_holder}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="iban">IBAN</label>
                                    <input id="iban" name="iban" type="text" value="{{$user->bank->iban}}" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="swift">SWIFT</label>
                                    <input id="swift" name="swift" type="text" value="{{$user->bank->swift}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_name">Name of Bank</label>
                                    <input id="bank_name" name="bank_name" type="text" value="{{$user->bank->bank_name}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_address">Bank Address</label>
                                    <input id="bank_address" name="bank_address" type="text" value="{{$user->bank->bank_address}}" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_phone">Bank Phone</label>
                                    <input id="bank_phone" name="bank_phone" type="text" value="{{$user->bank->bank_phone}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        @if($user->profile->type == 'Company')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_officer_name">Bank Officer Name</label>
                                    <input id="bank_officer_name" name="bank_officer_name" type="text" value="{{$user->bank->bank_officer_name}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_officer_email">Bank Officer Email Address</label>
                                    <input id="bank_officer_email" name="bank_officer_email" type="text" value="{{$user->bank->bank_officer_email}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        @endif
                        <br>
                        <h5>Bank Account2</h5>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="account_holder2">Account holder</label>
                                    <input id="account_holder2" name="account_holder2" type="text" value="{{$user->bank->account_holder2}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="iban2">IBAN</label>
                                    <input id="iban2" name="iban2" type="text" value="{{$user->bank->iban2}}" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="swift2">SWIFT</label>
                                    <input id="swift2" name="swift2" type="text" value="{{$user->bank->swift2}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_name2">Name of Bank</label>
                                    <input id="bank_name2" name="bank_name2" type="text" value="{{$user->bank->bank_name2}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_address2">Bank Address</label>
                                    <input id="bank_address2" name="bank_address2" type="text" value="{{$user->bank->bank_address2}}" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_phone2">Bank Phone</label>
                                    <input id="bank_phone2" name="bank_phone2" type="text" value="{{$user->bank->bank_phone2}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        @if($user->profile->type == 'Company')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_officer_name2">Bank Officer Name</label>
                                    <input id="bank_officer_name2" name="bank_officer_name2" type="text" value="{{$user->bank->bank_officer_name2}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_officer_email2">Bank Officer Email Address</label>
                                    <input id="bank_officer_email2" name="bank_officer_email2" type="text" value="{{$user->bank->bank_officer_email2}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        @endif
                        <a href="{{route('admin.users.index')}}" class="btn btn-rose">Cancel Edit</a>
                        <button type="submit" class="btn btn-success pull-right">Update Profile</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection
