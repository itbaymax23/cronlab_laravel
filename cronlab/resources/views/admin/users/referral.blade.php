@extends('layouts.admin')
@section('title', 'User Referral & Link')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="rose">
                    <i class="material-icons">contacts</i>
                </div>
                <br>
                <h4 class="card-title"> Total Referral</h4>

                <div class="card-content">
                    
                    <div class="table-responsive">
                        <button type="button" class="btn btn-raised btn-round btn-info pull-right" data-toggle="modal" data-target="#referralModal">Add Referred</button>
                        <!-- small modal -->
                        <div class="modal fade" id="referralModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-medium ">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h5>Add Referral code in User</h5>
                                    </div>
                                    <form action="{{route('admin.user.newRefer')}}" method="post">
                                        {{ csrf_field() }}
                                        @if(count($errors) > 0)
                                            <div class="alert alert-danger alert-with-icon" data-notify="container">
                                                <i class="material-icons" data-notify="icon">notifications</i>
                                                <span data-notify="message">
                                                    @foreach($errors->all() as $error)
                                                        <li><strong> {{$error}} </strong></li>
                                                    @endforeach
                                                </span>
                                            </div>
                                        @endif

                                        <div class="modal-body">
                                            <div class="form-group label-floating">
                                                <select class="selectpicker" name="user" data-style="btn btn-warning btn-round" data-size="5">
                                                    @foreach($users as $user)
                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label  class="control-label" for="referred_code">Start Code</label>
                                                        <input id="referred_code" name="referred_code" type="number" value="{{$referred_code}}" min="{{$referred_code}}" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label  class="control-label" for="count">Number of count</label>
                                                        <input id="count" name="count" type="number" value="1" min="1" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="modal-footer text-center">
                                            <button type="button" class="btn btn-simple" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-success btn-sm">Add</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <br>
                        <!--    end small modal -->
                        @if(count($referrals) > 0)
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center">SN</th>
                                    <th class="text-center">Photo</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Join Date</th>
                                    <th class="text-center">Referred Code</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $id=0;@endphp
                                @foreach($referrals as $referral)
                                    @php $id++;@endphp
                                    <tr>
                                        <td class="text-center">{{ $id }}</td>
                                        <td class="text-center" width="10%" >
                                            <img src="{{$referral->user->profile->avatar}}" class="img-circle" alt="No Photo"  >
                                        </td>
                                        <td class="text-center">{{$referral->user->name}}</td>
                                        <td class="text-center">{{ date('F jS, Y \a\t h:i a', strtotime($referral->user->created_at))}}</td>
                                        <td class="text-center">{{$referral->referred_code}}</td>
                                        <td class="text-center">

                                            @if($referral->status == 0)
                                                Not Active
                                            @else
                                                Active
                                            @endif

                                        </td>
                                        <td class="td-actions text-center">
                                            @if($referral->status == 1)
                                                <a href="#" type="button"
                                               rel="tooltip" class="btn btn-danger" disabled>Delete</a>
                                            @else
                                                <a href="{{route('admin.user.delRefer', $referral->id)}}" type="button"
                                               rel="tooltip" class="btn btn-danger" >Delete</a>
                                            @endif
                                            
                                        </td>
                                    </tr>

                                @endforeach


                                </tbody>
                            </table>

                        @else
                            <h2> The Members Doesn't have Any Referral.</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection