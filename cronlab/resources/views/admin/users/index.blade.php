@extends('layouts.admin')

@section('title', 'All Member Profile')

@section('content')

@php use App\Invest;@endphp

    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">face</i>
                </div>
                <div class="card-content">
                    <p class="category">Level1 Members</p>
                    <h4 class="card-title">{{$count->level1}}</h4>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">face</i>
                </div>
                <div class="card-content">
                    <p class="category">Level2 Members</p>

                    <h4 class="card-title">{{$count->level2}}</h4>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="red">
                    <i class="material-icons">face</i>
                </div>
                <div class="card-content">
                    <p class="category">Level3 Members</p>

                    <h4 class="card-title">{{$count->level3}}</h4>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="purple">
                    <i class="material-icons">face</i>
                </div>
                <div class="card-content">
                    <p class="category">Level4 Members</p>
                    <h4 class="card-title">{{$count->level4}}</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="rose">
                    <i class="material-icons">contacts</i>
                </div>
                <br>
                <h4 class="card-title">All Members</h4>
                <div class="card-content">
                    <br>
                    <div class="table-responsive">

                        <table class="table" id="all_members">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Photo</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Level</th>
                                <th class="text-center">Shares</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">View</th>
                                <th class="text-center">Edit</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if($users)

                                @php $id=0;@endphp

                                @foreach($users as $user)

                                    @php $id++;@endphp
                                    @php $investments = Invest::whereUser_id($user->id)->get();@endphp
                                    @php $investment_num = count($investments);@endphp

                                    <tr>
                                        <td class="text-center">{{ $id }}</td>
                                        <td width="10%">
                                            <img src="{{asset($user->profile->avatar)}}" class="img-circle"
                                                 alt="User Photo">
                                        </td>
                                        <td class="text-center">{{$user->name}}</td>
                                        <td class="text-center">{{$user->email}}</td>
                                        <td class="text-center">{{'Level '.$user->profile->level}}</td>
                                        <td class="text-center">{{$investment_num}}</td>
                                        <td class="text-center">
                                            @if($user->active == 0)
                                                Not Active
                                            @else
                                                Active
                                            @endif
                                        </td>
                                        <td class="td-actions text-center">
                                            <a href="{{route('admin.user.show', $user->id)}}" type="button"
                                               rel="tooltip" class="btn btn-success">Show</a>
                                        </td>
                                        <td class="td-actions text-center">
                                            <a href="{{route('admin.user.edit', $user->id)}}" type="button"
                                               rel="tooltip" class="btn btn-rose">Edit</a>
                                        </td>
                                        <td class="td-actions text-center">
                                            <a href="{{route('admin.user.delete', $user->id)}}" type="button"
                                               rel="tooltip" class="btn btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach

                            @endif

                            </tbody>
                        </table>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-5">

                            {{$users->appends(['s'=>$s])->render()}}

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-3">
                <div class="card card-content">
                    <div class="card-content">
                        <form action="{{route('admin.users.index')}}" method="get">
                            <div class="form-group label-floating">
                                <label for="s" class="control-label">Search</label>
                                <input type="text" id="s" name="s" value="{{isset($s) ? $s : ''}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary ">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
