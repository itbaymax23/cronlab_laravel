@extends('layouts.admin')

@section('title', 'Show User Verify Form Data')

@section('content')


    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="purple">
                    <i class="material-icons">perm_identity</i>
                </div>
                <div class="card-content">
                    <h4 class="card-title">Show Member Profile -
                        <small class="category">With User Verify Form Data</small>
                    </h4>

                    <form>
                        {{ csrf_field() }}
                        @if(count($errors) > 0)
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">notifications</i>
                            <span data-notify="message">

                                    @foreach($errors->all() as $error)
                                        <li><strong> {{$error}} </strong></li>
                                    @endforeach

                            </span>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="{{asset($verify->user->profile->avatar)}}" alt="...">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6">

                                <div class="form-group label-floating">

                                    <label  class="control-label" for="name">Full Name</label>
                                    <input id="name" name="name" type="text" value="{{$verify->user->name}}" disabled class="form-control">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="email">Email Address</label>
                                    <input id="email" name="email" value="{{$verify->user->email}}" type="text" disabled class="form-control">
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-6">

                                <div class="form-group label-floating">
                                    <label  class="control-label" for="occupation">Occupation</label>
                                    <input id="occupation" name="occupation" type="text" value="{{$verify->user->profile->occupation}}" disabled class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="country">Member Country</label>
                                    <input id="country" name="country" type="text" value="{{$verify->user->profile->country}}" disabled class="form-control">
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="address">Address Line 1</label>
                                    <input id="address" name="address" value="{{$verify->user->profile->address}}"  type="text" disabled class="form-control">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="address2">Address Line 2</label>
                                    <input id="address2" name="address2" value="{{$verify->user->profile->address2}}" type="text" disabled class="form-control">

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="city">City</label>
                                    <input id="city" name="city" type="text" value="{{$verify->user->profile->city}}" disabled class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="state">State</label>
                                    <input id="state" name="state" type="text" value="{{$verify->user->profile->state}}" disabled class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="postcode">Postal Code</label>
                                    <input id="postcode" name="postcode" type="text" value="{{$verify->user->profile->postcode}}" disabled class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="mobile">Mobile Number</label>
                                    <input id="mobile" name="mobile" type="text" value="{{$verify->user->profile->mobile}}" disabled class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="telephone">Telephone Number</label>
                                    <input id="telephone" name="telephone" type="text" value="{{$verify->user->profile->telephone}}" disabled class="form-control">
                                </div>
                            </div>
                        </div>
                        <br>
                        <h5>Bank Account1</h5>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="account_holder">Account holder</label>
                                    <input id="account_holder" name="account_holder" type="text" value="{{$verify->user->bank->account_holder}}" disabled class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="iban">IBAN</label>
                                    <input id="iban" name="iban" type="text" value="{{$verify->user->bank->iban}}" disabled class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="swift">SWIFT</label>
                                    <input id="swift" name="swift" type="text" value="{{$verify->user->bank->swift}}" disabled class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_name">Name of Bank</label>
                                    <input id="bank_name" name="bank_name" type="text" value="{{$verify->user->bank->bank_name}}" disabled class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_address">Bank Address</label>
                                    <input id="bank_address" name="bank_address" type="text" value="{{$verify->user->bank->bank_address}}" disabled class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_phone">Bank Phone</label>
                                    <input id="bank_phone" name="bank_phone" type="text" value="{{$verify->user->bank->bank_phone}}" disabled class="form-control">
                                </div>
                            </div>
                        </div>
                        @if($verify->user->profile->type == 'Company')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_officer_name">Bank Officer Name</label>
                                    <input id="bank_officer_name" name="bank_officer_name" type="text" value="{{$verify->user->bank->bank_officer_name}}" disabled class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_officer_email">Bank Officer Email Address</label>
                                    <input id="bank_officer_email" name="bank_officer_email" type="text" value="{{$verify->user->bank->bank_officer_email}}" disabled class="form-control">
                                </div>
                            </div>
                        </div>
                        @endif
                        <br>
                        <h5>Bank Account2</h5>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="account_holder2">Account holder</label>
                                    <input id="account_holder2" name="account_holder2" type="text" value="{{$verify->user->bank->account_holder2}}" disabled class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="iban2">IBAN</label>
                                    <input id="iban2" name="iban2" type="text" value="{{$verify->user->bank->iban2}}" disabled class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="swift2">SWIFT</label>
                                    <input id="swift2" name="swift2" type="text" value="{{$verify->user->bank->swift2}}" disabled class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_name2">Name of Bank</label>
                                    <input id="bank_name2" name="bank_name2" type="text" value="{{$verify->user->bank->bank_name2}}" disabled class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_address2">Bank Address</label>
                                    <input id="bank_address2" name="bank_address2" type="text" value="{{$verify->user->bank->bank_address2}}" disabled class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_phone2">Bank Phone</label>
                                    <input id="bank_phone2" name="bank_phone2" type="text" value="{{$verify->user->bank->bank_phone2}}" disabled class="form-control">
                                </div>
                            </div>
                        </div>
                        @if($verify->user->profile->type == 'Company')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_officer_name2">Bank Officer Name</label>
                                    <input id="bank_officer_name2" name="bank_officer_name2" type="text" value="{{$verify->user->bank->bank_officer_name2}}" disabled class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label  class="control-label" for="bank_officer_email2">Bank Officer Email Address</label>
                                    <input id="bank_officer_email2" name="bank_officer_email2" type="text" value="{{$verify->user->bank->bank_officer_email2}}" disabled class="form-control">
                                </div>
                            </div>
                        </div>
                        @endif
                        <a href="{{route('adminKyc2Reject',['id'=>$verify->id])}}" class="btn btn-danger">Reject Request</a>
                        <a href="{{route('adminKyc2Accept',['id'=>$verify->id])}}" class="btn btn-success pull-right">Accept Request</a>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-4">
                <div class="card card-content">
                    <div class="card-content">
                        <div class="alert alert-primary">
                            <h4 class="card-title text-center"><span>Address Data</span></h4>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                <label  class="control-label" for="country">Document Type</label>
                                <input type="text" value="{{$verify->name}}" disabled class="form-control">
                            </div>
                        </div>
                        <br>
                        <h5 class="text-center text-info"> Page Photo</h5>

                        <br>


                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="fileinput fileinput-new text-center">
                                    <div class="fileinput-new thumbnail">
                                        <img src="{{$verify->photo}}" alt="...">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>


                    </div>

                </div>
            </div>


        </div>
    </div>

@endsection