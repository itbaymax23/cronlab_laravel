<div class="sidebar" data-active-color="rose" data-background-color="black" data-image="{{asset('img/sidebar-1.jpg')}}">
    <!--
Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
Tip 2: you can also add an image using data-image tag
Tip 3: you can change the color of the sidebar with data-background-color="white | black"
-->
    <div class="logo">
        <a href="{{route('adminIndex')}}" class="simple-text">
            Admin Panel
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="#" class="simple-text">
            TPA
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{asset(Auth::user()->profile->avatar)}}" alt="User Photo" class="img">
            </div>
            <div class="info">
                <a data-toggle="collapse" href="" class="collapsed">
                    {{ Auth::user()->name }}
                </a>

            </div>
        </div>
        <ul class="nav">
            <li class="{{ Request::is('admin/dashboard') ? "active" :"" }}">
                <a href="{{route('adminIndex')}}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>

            <li class="{{ Request::is('admin/mail/inbox') || Request::is('admin/email/compose') || Request::is('admin/message/compose') ? "active" :"" }}">
                <a data-toggle="collapse" href="#UserMail">
                    <i class="material-icons">markunread_mailbox</i>
                    <p>Email System
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="UserMail">
                    <ul class="nav">
                        <li class="{{ Request::is('admin/mail/inbox') ? "active" :"" }}">
                            <a href="{{route('adminEmail')}}">Inbox</a>
                        </li>
                        <li class="{{ Request::is('admin/email/compose') ? "active" :"" }}">
                            <a href="{{route('adminEmail.create')}}">Compose Email</a>
                        </li>
                        <li class="{{ Request::is('admin/message/compose') ? "active" :"" }}">
                            <a href="{{route('adminMessage.create')}}">Compose Message</a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="{{ Request::is('admin/users') || Request::is('admin/verified/users') || Request::is('admin/unverified/users') || Request::is('admin/banned/users') || Request::is('admin/user/referrals') || Request::is('admin/user/create') ? "active" :"" }}">
                <a data-toggle="collapse" href="#UserMember">
                    <i class="material-icons">face</i>
                    <p>Member
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="UserMember">
                    <ul class="nav">
                        <li class="{{ Request::is('admin/users') ? "active" :"" }}">
                            <a href="{{route('admin.users.index')}}">All Member</a>
                        </li>
                        <li class="{{ Request::is('admin/verified/users') ? "active" :"" }}">
                            <a href="{{route('admin.users.verified')}}">All Active</a>
                        </li>
                        <li class="{{ Request::is('admin/unverified/users') ? "active" :"" }}">
                            <a href="{{route('admin.users.unverified')}}">All Unverified</a>
                        </li>
                        <li class="{{ Request::is('admin/banned/users') ? "active" :"" }}">
                            <a href="{{route('admin.users.banned')}}">All Suspended</a>
                        </li>

                        <li class="{{ Request::is('admin/user/referrals') ? "active" :"" }}">
                            <a href="{{route('admin.user.referShow')}}">All Referred</a>
                        </li>
                        <li class="{{ Request::is('admin/user/create') ? "active" :"" }}">
                            <a href="{{route('admin.user.create')}}">Create Member</a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="{{ Request::is('admin/posts') || Request::is('admin/posts/create') || Request::is('admin/categories') || Request::is('admin/tags') ? "active" :"" }}">
                <a data-toggle="collapse" href="#BlogArea">
                    <i class="material-icons">announcement</i>
                    <p>News Section
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="BlogArea">
                    <ul class="nav">


                        <li class="{{ Request::is('admin/posts') || Request::is('admin/posts/create') ? "active" :"" }}">
                            <a data-toggle="collapse" href="#BlogPosts">
                                <i class="material-icons">surround_sound</i>
                                <p>News
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <div class="collapse" id="BlogPosts">
                                <ul class="nav">
                                    <li class="{{ Request::is('admin/posts') ? "active" :"" }}">
                                        <a href="{{route('admin.posts.index')}}">All News</a>
                                    </li>

                                    <li class="{{ Request::is('admin/posts/create') ? "active" :"" }}">
                                        <a href="{{route('admin.post.create')}}">Create News</a>
                                    </li>

                                </ul>
                            </div>
                        </li>

                        <li class="{{ Request::is('admin/categories') ? "active" :"" }}">
                            <a href="{{route('admin.category.index')}}">
                                <i class="material-icons">build</i>
                                <p>Categories</p>
                            </a>
                        </li>

                        <li class="{{ Request::is('admin/tags') ? "active" :"" }}">
                            <a href="{{route('admin.tags.index')}}">
                                <i class="material-icons">perm_media</i>
                                <p>Tags</p>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>

            <li class="{{ Request::is('admin/invest/plan') || Request::is('admin/invest/plan/create') || Request::is('admin/invest/style') ? "active" :"" }}">
                <a data-toggle="collapse" href="#InvestArea">
                    <i class="material-icons">whatshot</i>
                    <p>Investment Section
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="InvestArea">
                    <ul class="nav">


                        <li class="{{ Request::is('admin/invest/plan') || Request::is('admin/invest/plan/create') ? "active" :"" }}">
                            <a data-toggle="collapse" href="#PlanArea">
                                <i class="material-icons">surround_sound</i>
                                <p>Plan
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <div class="collapse" id="PlanArea">

                                <ul class="nav">
                                    <li class="{{ Request::is('admin/invest/plan') ? "active" :"" }}">
                                        <a href="{{route('adminInvest')}}">All Plan</a>
                                    </li>

                                    <li class="{{ Request::is('admin/invest/plan/create') ? "active" :"" }}">
                                        <a href="{{route('adminInvest.create')}}">Create Plan</a>
                                    </li>

                                </ul>
                            </div>
                        </li>

                        <li class="{{ Request::is('admin/invest/style') ? "active" :"" }}">
                            <a href="{{route('adminStyle')}}">
                                <i class="material-icons">build</i>
                                <p>Style</p>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>

            <li class="{{ Request::is('admin/local/gateways') || Request::is('admin/local/gateway/create') ? "active" :"" }}">
                <a data-toggle="collapse" href="#lGateways">
                    <i class="material-icons">transfer_within_a_station</i>
                    <p>Offline Gateway
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="lGateways">
                    <ul class="nav">

                        <li class="{{ Request::is('admin/local/gateways') ? "active" :"" }}">
                            <a href="{{route('admin.gateways.local')}}">All Local Gateways</a>
                        </li>

                        <li class="{{ Request::is('admin/local/gateway/create') ? "active" :"" }}">
                            <a href="{{route('admin.local.create')}}">Create Local Gateway</a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="{{ Request::is('admin/memberships') || Request::is('admin/membership/create') ? "active" :"" }}">
                <a data-toggle="collapse" href="#membership">
                    <i class="material-icons">settings_input_antenna</i>
                    <p>Membership
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="membership">
                    <ul class="nav">
                        <li class="{{ Request::is('admin/memberships') ? "active" :"" }}">
                            <a href="{{route('admin.memberships.index')}}">All Membership</a>
                        </li>
                        <li class="{{ Request::is('admin/membership/create') ? "active" :"" }}">
                            <a href="{{route('admin.membership.create')}}">Create Membership</a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="{{ Request::is('admin/users/deposit') || Request::is('admin/users/deposit/local') ? "active" :"" }}">
                <a data-toggle="collapse" href="#DepositArea">
                    <i class="material-icons">payment</i>
                    <p>Deposit Area
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="DepositArea">
                    <ul class="nav">
                        <li class="{{ Request::is('admin/users/deposit') ? "active" :"" }}">
                            <a href="{{route('admin.users.deposit')}}">System Deposit

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/users/deposit/local') ? "active" :"" }}">
                            <a href="{{route('admin.deposit.local')}}">Deposit Request</a>
                        </li>

                    </ul>
                </div>
            </li>

            <li class="{{ Request::is('admin/users/withdraws') || Request::is('admin/users/withdraws/request') ? "active" :"" }}">
                <a data-toggle="collapse" href="#WithdrawArea">
                    <i class="material-icons">account_balance</i>
                    <p>Withdraw Area
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="WithdrawArea">
                    <ul class="nav">
                        <li class="{{ Request::is('admin/users/withdraws') ? "active" :"" }}">
                            <a href="{{route('admin.users.withdraws')}}">Completed Withdraw

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/users/withdraws/request') ? "active" :"" }}">
                            <a href="{{route('admin.withdraws.request')}}">Withdraw Request</a>
                        </li>

                    </ul>
                </div>
            </li>

            <li class="{{ Request::is('admin/user/supports') || Request::is('admin/user/supports/close') ? "active" :"" }}">
                <a data-toggle="collapse" href="#SupportArea">
                    <i class="material-icons">supervisor_account</i>
                    <p>Support Area
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="SupportArea">
                    <ul class="nav">
                        <li class="{{ Request::is('admin/user/supports') ? "active" :"" }}">
                            <a href="{{route('adminSupports.open')}}">All Open Ticket
                            </a>
                        </li>
                        <li class="{{ Request::is('admin/user/supports/close') ? "active" :"" }}">
                            <a href="{{route('adminSupports.index')}}">All Close Ticket</a>
                        </li>

                    </ul>
                </div>
            </li>

            <li class="{{ Request::is('admin/kyc/identity') || Request::is('admin/kyc/address') || Request::is('admin/kyc/bank') ? "active" :"" }}">
                <a data-toggle="collapse" href="#KycArea">
                    <i class="material-icons">supervisor_account</i>
                    <p>KYC Area
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="KycArea">
                    <ul class="nav">
                        <li class="{{ Request::is('admin/kyc/identity') ? "active" :"" }}">
                            <a href="{{route('adminKyc')}}">Identity Verify Request
                            </a>
                        </li>
                        <li class="{{ Request::is('admin/kyc/address') ? "active" :"" }}">
                            <a href="{{route('adminKyc2')}}">Address Verify Request</a>
                        </li>
                        <li class="{{ Request::is('admin/kyc/company') ? "active" :"" }}">
                            <a href="{{route('adminKyc3')}}">Company Verify Request</a>
                        </li>

                    </ul>
                </div>
            </li>

            <li class="{{ Request::is('admin/user/reviews') || Request::is('admin/faqs/index') || Request::is('admin/website/pages') ? "active" :"" }}">
                <a data-toggle="collapse" href="#SiteTool">
                    <i class="material-icons">settings_input_component</i>
                    <p>Website Toolkit
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="SiteTool">
                    <ul class="nav">
                        <li class="{{ Request::is('admin/user/reviews') ? "active" :"" }}">
                            <a href="{{route('adminReview')}}">Testimonials</a>
                        </li>
                        <li class="{{ Request::is('admin/faqs/index') ? "active" :"" }}">
                            <a href="{{route('adminFAQ')}}">Website F.A.Q</a>
                        </li>
                        <li class="{{ Request::is('admin/website/pages') ? "active" :"" }}">
                            <a href="{{route('adminPages')}}">Website Page</a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="{{ Request::is('admin/website/settings') ? "active" :"" }}">
                <a href="{{route('websiteSettings')}}">
                    <i class="material-icons">settings</i>
                    <p>Settings
                    </p>
                </a>
            </li>

        </ul>
    </div>
</div>