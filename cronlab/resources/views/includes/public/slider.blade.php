<!--     *********     HEADER 3      *********      -->
<div class="carousel-inner">
    <div class="item active">
        <div class="page-header header-filter" style="background-image: url('{{asset('img/dg1.jpg')}}');">

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-left">
                        <h1 class="title">Wellcome to {{config('app.name')}}</h1>
                        <h4>It's time to change...</h4>
                        <br />

                        <div class="buttons">

                            @if(Auth::guest())

                            <a href="{{ route('register') }}" class="btn btn-primary btn-lg">
                                Register Now
                            </a>

                            @else

                                <a href="{{ route('userDashboard') }}" class="btn btn-info btn-lg">
                                   View Ads Now
                                </a>

                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="item">
        <div class="page-header header-filter" style="background-image: url('{{asset('img/dg2.jpg')}}');">

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-left">
                        <h1 class="title">Wellcome to {{config('app.name')}}</h1>
                        <h4>It's time to change...</h4>

                        <br />
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="item">
        <div class="page-header header-filter" style="background-image: url('{{asset('img/dg3.jpg')}}');">

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-left">
                        <h1 class="title">Wellcome to {{config('app.name')}}</h1>
                        <h4>It's time to change...</h4>
                        
                        <br />

                        <div class="buttons">

                            @if(Auth::guest())
                            <a href="{{ route('login') }}" class="btn btn-warning btn-lg">
                                <i class="material-icons">account_box</i> Login Now
                            </a>
                            @else
                                <a href="" class="btn btn-rose btn-lg">
                                    <i class="material-icons">account_box</i> My Referrals
                                </a>
                            @endif

                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

</div>