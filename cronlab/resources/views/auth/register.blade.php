
@extends('layouts.app')
@section('title', 'Sign Up For Earn Cash')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div class="card card-signup">

                    <div class="header header-info text-center">
                        <h4 class="card-title">Sign Up For Full Features</h4>
                    </div>
                    
                    <div class="row">
                        <div class="card-content">
                            <div class="mt-radio-inline">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 text-center form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="optionsRadios" id="optionsRadios1" /> 
                                            <span style="font-size: 20px;"> Person</span>
                                        </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 text-center form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="optionsRadios" id="optionsRadios2" />
                                            <span style="font-size: 20px;"> Company</span>
                                        </label>
                                    </div>
                                </div>
                                        
                                        
                            </div>

                            <form id="person_register" class="form-horizontal" method="POST" action="{{ route('register') }}">

                                {{ csrf_field() }}
                                @if(count($errors) > 0)
                                    <div class="alert alert-danger alert-with-icon" data-notify="container">
                                        <i class="material-icons" data-notify="icon">notifications</i>
                                        <span data-notify="message">

                                            @foreach($errors->all() as $error)
                                                <li><strong> {{$error}} </strong></li>
                                            @endforeach

                                    </span>
                                    </div>
                                @endif


                                <input id="optionRadio1" type="text" name="type" value="Person" hidden>
                                <input id="bank_officer_name" type="text" name="bank_officer_name" value="BankOfficerName" hidden>
                                <input id="bank_officer_email" type="text" name="bank_officer_email" value="BankOfficer@email.com" hidden>
                                
                                <div class="card-content">
                                    <div class="input-group {{ $errors->has('referred_code') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">redeem</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="referred_code">Referred Code</label>
                                            <input id="referred_code" type="text" class="form-control" name="referred_code" required autofocus>

                                            @if ($errors->has('referred_code'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('referred_code') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('name') ? ' has-error' : '' }}">
										<span class="input-group-addon">
											<i class="material-icons">face</i>
										</span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="name">Full Name</label>
                                            <input id="name" type="text" class="form-control" name="name" required autofocus>

                                            @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('state') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">room</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="state">State</label>
                                            <input id="state" type="text" class="form-control" name="state" required autofocus>

                                            @if ($errors->has('state'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">room</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="city">City</label>
                                            <input id="city" type="text" class="form-control" name="city" required autofocus>

                                            @if ($errors->has('city'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">room</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="address">Address</label>
                                            <input id="address" type="text" class="form-control" name="address" required autofocus>

                                            @if ($errors->has('address'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('postcode') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">room</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="postcode">Postal Code</label>
                                            <input id="postcode" type="text" class="form-control" name="postcode" required autofocus>

                                            @if ($errors->has('postcode'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('postcode') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">phone</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="telephone">Telephone Number</label>
                                            <input id="telephone" type="text" class="form-control" name="telephone" required autofocus>

                                            @if ($errors->has('telephone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('telephone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
										<span class="input-group-addon">
											<i class="material-icons">email</i>
										</span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="email">Email Address</label>
                                            <input id="email" type="email" class="form-control" name="email" required autofocus>

                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="password">Password</label>
                                            <input id="password" type="password" class="form-control" name="password" required>

                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="password-confirm">Password Confirm</label>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                        </div>
                                    </div>

                                    <hr>
                                    <h4 class="strong text-center">Bank Account</h4>

                                    <div class="input-group {{ $errors->has('account_holder') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">account_balance</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="account_holder">Account holder</label>
                                            <input id="account_holder" type="text" class="form-control" name="account_holder" required autofocus>

                                            @if ($errors->has('account_holder'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('account_holder') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('iban') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">assignment_ind</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="iban">IBAN</label>
                                            <input id="iban" type="text" class="form-control" name="iban" required autofocus>

                                            @if ($errors->has('iban'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('iban') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('swift') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">assignment_ind</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="swift">SWIFT</label>
                                            <input id="swift" type="text" class="form-control" name="swift" required autofocus>

                                            @if ($errors->has('swift'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('swift') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('bank_name') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">account_balance_wallet</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="bank_name">Name of the Bank</label>
                                            <input id="bank_name" type="text" class="form-control" name="bank_name" required autofocus>

                                            @if ($errors->has('bank_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('bank_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('bank_address') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">room</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="bank_address">Bank Address</label>
                                            <input id="bank_address" type="text" class="form-control" name="bank_address" required autofocus>

                                            @if ($errors->has('bank_address'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('bank_address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('bank_phone') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">phone</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="bank_phone">Bank Phone</label>
                                            <input id="bank_phone" type="text" class="form-control" name="bank_phone" required autofocus>

                                            @if ($errors->has('bank_phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('bank_phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-info">
                                        <i class="material-icons">input</i> Register Now
                                    </button>

                                    <a class="btn btn-warning" href="{{ route('login') }}">
                                        <i class="material-icons">warning</i> Already Account? Login Here
                                    </a>
                                </div>
                            </form>

                            <form id="company_register" class="form-horizontal" method="POST" action="{{ route('register') }}" hidden>

                                {{ csrf_field() }}
                                @if(count($errors) > 0)
                                    <div class="alert alert-danger alert-with-icon" data-notify="container">
                                        <i class="material-icons" data-notify="icon">notifications</i>
                                        <span data-notify="message">

                                            @foreach($errors->all() as $error)
                                                <li><strong> {{$error}} </strong></li>
                                            @endforeach

                                    </span>
                                    </div>
                                @endif


                                <input id="optionRadio2" type="text" name="type" value="Company" hidden>
                                <div class="card-content">
                                    <div class="input-group {{ $errors->has('referred_code') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">redeem</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="referred_code">Referred Code</label>
                                            <input id="referred_code" type="text" class="form-control" name="referred_code" required autofocus>

                                            @if ($errors->has('referred_code'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('referred_code') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">business</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="name">Company Name</label>
                                            <input id="name" type="text" class="form-control" name="name" required autofocus>

                                            @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('state') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">room</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="state">State</label>
                                            <input id="state" type="text" class="form-control" name="state" required autofocus>

                                            @if ($errors->has('state'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">room</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="city">City</label>
                                            <input id="city" type="text" class="form-control" name="city" required autofocus>

                                            @if ($errors->has('city'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">room</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="address">Address</label>
                                            <input id="address" type="text" class="form-control" name="address" required autofocus>

                                            @if ($errors->has('address'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('postcode') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">room</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="postcode">Postal Code</label>
                                            <input id="postcode" type="text" class="form-control" name="postcode" required autofocus>

                                            @if ($errors->has('postcode'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('postcode') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">phone</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="telephone">Company Telephone Number</label>
                                            <input id="telephone" type="text" class="form-control" name="telephone" required autofocus>

                                            @if ($errors->has('telephone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('telephone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="email">Email Address of the Company</label>
                                            <input id="email" type="email" class="form-control" name="email" required autofocus>

                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="password">Password</label>
                                            <input id="password" type="password" class="form-control" name="password" required>

                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="password-confirm">Password Confirm</label>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                        </div>
                                    </div>

                                    <hr>
                                    <h4 class="strong text-center">Bank Account</h4>

                                    <div class="input-group {{ $errors->has('account_holder') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">account_balance</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="account_holder">Account holder</label>
                                            <input id="account_holder" type="text" class="form-control" name="account_holder" required autofocus>

                                            @if ($errors->has('account_holder'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('account_holder') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('iban') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">assignment_ind</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="iban">IBAN</label>
                                            <input id="iban" type="text" class="form-control" name="iban" required autofocus>

                                            @if ($errors->has('iban'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('iban') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('swift') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">assignment_ind</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="swift">SWIFT</label>
                                            <input id="swift" type="text" class="form-control" name="swift" required autofocus>

                                            @if ($errors->has('swift'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('swift') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('bank_name') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">account_balance_wallet</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="bank_name">Name of the Bank</label>
                                            <input id="bank_name" type="text" class="form-control" name="bank_name" required autofocus>

                                            @if ($errors->has('bank_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('bank_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('bank_address') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">room</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="bank_address">Bank Address</label>
                                            <input id="bank_address" type="text" class="form-control" name="bank_address" required autofocus>

                                            @if ($errors->has('bank_address'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('bank_address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('bank_phone') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">phone</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="bank_phone">Bank Phone</label>
                                            <input id="bank_phone" type="text" class="form-control" name="bank_phone" required autofocus>

                                            @if ($errors->has('bank_phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('bank_phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('bank_officer_name') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">account_circle</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="bank_officer_name">Bank Officer Name</label>
                                            <input id="bank_officer_name" type="text" class="form-control" name="bank_officer_name" required autofocus>

                                            @if ($errors->has('bank_officer_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('bank_officer_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="input-group {{ $errors->has('bank_officer_email') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="bank_officer_email">Bank Officer E-Mail</label>
                                            <input id="bank_officer_email" type="text" class="form-control" name="bank_officer_email" required autofocus>

                                            @if ($errors->has('bank_officer_email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('bank_officer_email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-info">
                                        <i class="material-icons">input</i> Register Now
                                    </button>

                                    <a class="btn btn-warning" href="{{ route('login') }}">
                                        <i class="material-icons">warning</i> Already Account? Login Here
                                    </a>
                                </div>
                            </form>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection
