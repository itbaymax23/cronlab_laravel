<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    //
    protected $fillable = [
        'user_id',
        'account_holder',
        'iban',
        'swift',
        'bank_name',
        'bank_address',
        'bank_phone',
        'bank_officer_name',
        'bank_officer_email',
        'account_holder2',
        'iban2',
        'swift2',
        'bank_name2',
        'bank_address2',
        'bank_phone2',
        'bank_officer_name2',
        'bank_officer_email2',

    ];


    public function user(){

        return $this->belongsTo('App\User');

    }
}
