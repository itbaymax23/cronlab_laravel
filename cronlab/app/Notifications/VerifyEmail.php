<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class VerifyEmail extends Notification
{
    use Queueable;
    public $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        //
        $this->user = $user;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $data = $this->user;

        if ($data->profile->type == 'Person') {
            return (new MailMessage)
                        ->subject('Verify Your Account')
                        ->line('Your account has been successfully created.')
                        ->line('The information of your account is as follows.')
                        ->line('Full Name: ' . $data->name)
                        ->line('Email: ' . $data->email)
                        ->line('Address: ' . $data->profile->address)
                        ->line('Telephone Number: ' . $data->profile->telephone)
                        ->line('Account holder: ' . $data->bank->account_holder)
                        ->line('IBAN: ' . $data->bank->iban . ',  SWIFT: ' . $data->bank->swift)
                        ->line('Bank Name: ' . $data->bank->bank_name)
                        ->line('Bank Address: ' . $data->bank->bank_address)
                        ->line('Bank Phone: ' . $data->bank->bank_phone)
                        ->line('Please Verify Email Account To Continue.')
                        ->action('Click Here To Verify Email and Active Account', route('verify',$this->user->token))
                        ->line('Thank you for using our website!');
        } else {
            return (new MailMessage)
                        ->subject('Verify Your Account')
                        ->line('Your account has been successfully created.')
                        ->line('The information of your account is as follows.')
                        ->line('Company Name: ' . $data->name)
                        ->line('Company Email: ' . $data->email)
                        ->line('Company Address: ' . $data->profile->address)
                        ->line('Company Telephone Number: ' . $data->profile->telephone)
                        ->line('Account holder: ' . $data->bank->account_holder)
                        ->line('IBAN: ' . $data->bank->iban . ',  SWIFT: ' . $data->bank->swift)
                        ->line('Bank Name: ' . $data->bank->bank_name)
                        ->line('Bank Address: ' . $data->bank->bank_address)
                        ->line('Bank Phone: ' . $data->bank->bank_phone)
                        ->line('Bank Officer Name: ' . $data->bank->bank_officer_name)
                        ->line('Bank Officer Email: ' . $data->bank->bank_officer_email)
                        ->line('Please Verify Email Account To Continue.')
                        ->action('Click Here To Verify Email and Active Account', route('verify',$this->user->token))
                        ->line('Thank you for using our website!');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
