<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserReferred;
use App\Profile;
use App\Bank;
use App\Reflink;
use App\Referral;
use App\Http\Controllers\Controller;
use App\Settings;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/verify/logout';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            
            'referred_code'         => 'required|exists:referrals',
            'name'                  => 'required|string|max:191|unique:users',
            'email'                 => 'required|string|email|max:191|unique:users',
            'state'                 => 'required|string|max:191',
            'city'                  => 'required|string|max:191',
            'address'               => 'required|string|max:191',
            'postcode'              => 'required|string|max:191',
            'telephone'             => 'required|string|min:6|max:191',
            'password'              => 'required|string|min:6|confirmed',
            'account_holder'        => 'required|string|max:191',
            'iban'                  => 'required|string|max:191',
            'swift'                 => 'required|string|max:191',
            'bank_name'             => 'required|string|max:191',
            'bank_address'          => 'required|string|max:191',
            'bank_phone'            => 'required|string|max:191',
            'bank_officer_name'     => 'required|string|max:191',
            'bank_officer_email'    => 'required|string|email|max:191',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $equ_referral = Referral::where('referred_code', $data['referred_code'])->first();

            $referral = Referral::whereId($equ_referral->id)->first();
            $referral->status = 1;
            $referral->save();

            $parent_level = Profile::whereUser_id($equ_referral->user_id)->first();
            if ($parent_level->level == 4) {
                $level = $parent_level->level;
            } else {
                $level = $parent_level->level + 1;
            }

            $settings = Settings::first();

            if ($data['type'] == 'Person') {
                $user = User::create([

                    'name'              => $data['name'],
                    'email'             => $data['email'],
                    'password'          => bcrypt($data['password']),
                    'admin'             => 0,
                    'active'            => 0,
                    'membership_id'     => 1,
                    'membership_started'=> date('Y-m-d'),
                    'membership_expired'=> '2020-12-31',
                    'token'             => str_random(25),
                    'bonus'             => Carbon::now(),

                ]);

                Reflink::create([

                    'user_id'   => $user->id,
                    'link'      => $data['referred_code'],

                ]);

                Profile::create([

                    'user_id'       => $user->id,
                    'avatar'        => 'uploads/avatars/default.jpg',
                    'main_balance'  => $settings->signup_bonus,
                    'type'          => $data['type'],
                    'level'         => $level,
                    'telephone'     => $data['telephone'],
                    'state'         => $data['state'],
                    'city'          => $data['city'],
                    'address'       => $data['address'],
                    'postcode'      => $data['postcode']
                    
                ]);

                Bank::create([

                    'user_id'               => $user->id,
                    'account_holder'        => $data['account_holder'],
                    'iban'                  => $data['iban'],
                    'swift'                 => $data['swift'],
                    'bank_name'             => $data['bank_name'],
                    'bank_address'          => $data['bank_address'],
                    'bank_phone'            => $data['bank_phone']
                    
                ]);

            } else {

                $user = User::create([

                    'name'              => $data['name'],
                    'email'             => $data['email'],
                    'password'          => bcrypt($data['password']),
                    'admin'             => 0,
                    'active'            => 0,
                    'membership_id'     => 1,
                    'membership_started'=> date('Y-m-d'),
                    'membership_expired'=> '2020-12-31',
                    'token'             => str_random(25),
                    'bonus'             => Carbon::now(),

                ]);

                Reflink::create([

                    'user_id'   => $user->id,
                    'link'      => $data['referred_code'],

                ]);

                Profile::create([

                    'user_id'       => $user->id,
                    'avatar'        => 'uploads/avatars/default.jpg',
                    'main_balance'  => $settings->signup_bonus,
                    'type'          => $data['type'],
                    'level'         => $level,
                    'telephone'     => $data['telephone'],
                    'state'         => $data['state'],
                    'city'          => $data['city'],
                    'address'       => $data['address'],
                    'postcode'      => $data['postcode']
                    
                ]);

                Bank::create([

                    'user_id'               => $user->id,
                    'account_holder'        => $data['account_holder'],
                    'iban'                  => $data['iban'],
                    'swift'                 => $data['swift'],
                    'bank_name'             => $data['bank_name'],
                    'bank_address'          => $data['bank_address'],
                    'bank_phone'            => $data['bank_phone'],
                    'bank_officer_name'     => $data['bank_officer_name'],
                    'bank_officer_email'    => $data['bank_officer_email'],
                    
                ]);
            }

            $user->sendVerificationEmail();

            event(new UserReferred(request()->cookie('ref'), $user));

            session()->flash('message', 'Dear user your account create successful but not active. To active your account please check your email for verify.');
            Session::flash('type', 'warning');
            Session::flash('title', 'Email Verify Required');

            return $user;
    }
}
