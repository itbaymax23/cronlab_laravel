<?php

namespace App\Http\Controllers;

use App\Deposit;
use App\Invest;
use App\Interest;
use App\InterestLog;
use Carbon\Carbon;
use App\Kyc;
use App\Kyc2;
use App\Kyc3;
use App\Plan;
use App\Profile;
use App\Settings;
use App\Testimonial;
use App\User;
use App\Withdraw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    //
    public function __construct()
    {

        $this->middleware('admin');

    }

    public function index()
    {
        $deposit_notify = Deposit::whereStatus(0)->get();
        $withdraw_notify = Withdraw::whereStatus(0)->get();
        $kyc_notify = Kyc::whereStatus(0)->get();
        $kyc2_notify = Kyc2::whereStatus(0)->get();
        $kyc3_notify = Kyc3::whereStatus(0)->get();

        $earn = Profile::select('main_balance')->sum('main_balance');
        $amount = Invest::select('amount')->sum('amount') * Plan::find(1)->minimum - InterestLog::select('amount')->sum('amount');
        $invest = Invest::select('amount')->sum('amount') * Plan::find(1)->minimum;
        $bonus = InterestLog::select('amount')->sum('amount');

        $count = (object) array(

            "total"=>User::all()->count(),
            "active"=>User::whereActive(1)->count(),
            "banned"=>User::whereBan(1)->count(),
            "unverified"=>User::whereActive(0)->count(),
            "level1"=>Profile::whereLevel(1)->count(),
            "level2"=>Profile::whereLevel(2)->count(),
            "level3"=>Profile::whereLevel(3)->count(),
            "level4"=>Profile::whereLevel(4)->count(),
        );

        return view('admin.index',compact('deposit_notify','withdraw_notify',
            'kyc_notify','kyc2_notify','kyc3_notify','amount','invest','bonus','count'));
    }

    public function userDeposits()
    {
        $deposits= Deposit::where('status', '>=', 1)->orderBy('updated_at','desc')->get();

        $settings = Settings::first();

        return view('admin.deposit.index',compact('deposits','settings'));

    }

    public function userWithdraws()
    {
        $withdraws= Withdraw::where('status', '>=', 1)->orderBy('updated_at','desc')->paginate(20);

        $settings = Settings::first();

        return view('admin.withdraw.index',compact('withdraws','settings'));
    }

    public function userWithdrawsRequest()
    {
        $withdraws= Withdraw::whereStatus(0)->orderBy('created_at','desc')->paginate(20);

        $settings = Settings::first();

        return view('admin.withdraw.request',compact('withdraws','settings'));
    }

    public function localDeposits()
    {
        $deposits= Deposit::whereStatus(0)->orderBy('created_at','desc')->get();

        $settings = Settings::first();

        return view('admin.deposit.local',compact('deposits','settings'));
    }

    public function localDepositsUpdate($id)
    {
        $deposit= Deposit::find($id);

        $user = $deposit->user;

        $user->profile->deposit_balance = $user->profile->deposit_balance +  $deposit->net_amount;

        $user->profile->save();

        $deposit->status = 1;

        $deposit->save();



        session()->flash('message', 'User Deposit Request Has Been Successfully Approved');
        Session::flash('type', 'success');
        Session::flash('title', 'Deposit Approved');

        return redirect()->back();
    }

    public function localDepositsFraud($id)
    {
        $deposit= Deposit::find($id);
        $deposit->status = 2;
        $deposit->save();
        session()->flash('message', 'User Deposit Has Been Successfully Set As Fraud');
        Session::flash('type', 'success');
        Session::flash('title', 'Fraud Successfully');

        return redirect()->back();
    }

    public function payment($id)
    {
        $withdraw= Withdraw::find($id);

        $withdraw->status = 1;

        $withdraw->save();

        session()->flash('message', 'User Withdraw Request Has Been Successfully Approved');
        Session::flash('type', 'success');
        Session::flash('title', 'Withdraw Approved');

        return redirect()->back();
    }

    public function withdrawFraud($id)
    {
        $withdraw= Withdraw::find($id);

        $withdraw->status = 2;
        $withdraw->save();

        $user =  $withdraw->user;

        $user->profile->main_balance = $user->profile->main_balance + $withdraw->amount;

        $user->profile->save();

        session()->flash('message', 'User Withdraw Has Been Successfully Refund');
        Session::flash('type', 'success');
        Session::flash('title', 'Refund Successfully');

        return redirect()->back();
    }

    public function review()
    {
        $reviews= Testimonial::latest()->paginate(20);

        return view('admin.testimonials.index',compact('reviews'));
    }

    public function reviewPublish($id)
    {
        $review= Testimonial::find($id);

        $review->status = 1;

        $review->save();

        session()->flash('message', 'User Review Has Been Successfully Published');
        Session::flash('type', 'success');
        Session::flash('title', 'Published Successfully');

        return redirect()->back();
    }

    public function reviewUnPublish($id)
    {
        $review= Testimonial::find($id);

        $review->status = 0;

        $review->save();

        session()->flash('message', 'User Review Has Been Successfully Un-Published');
        Session::flash('type', 'success');
        Session::flash('title', 'Un-Published Successfully');

        return redirect()->back();
    }
}
