<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Deposit;
use App\Interest;
use App\InterestLog;
use App\Invest;
use App\Notifications\AdminCreateUser;
use App\Profile;
use App\Bank;
use App\Referral;
use App\Reflink;
use App\Share;
use App\TransferLog;
use App\User;
use App\Settings;
use App\UserLog;
use App\Video;
use App\Withdraw;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminUsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $s = $request->input('s');

        $users = User::oldest()->search($s)->paginate(10);
        
        $count = (object) array(
            "level1"=>Profile::whereLevel(1)->count(),
            "level2"=>Profile::whereLevel(2)->count(),
            "level3"=>Profile::whereLevel(3)->count(),
            "level4"=>Profile::whereLevel(4)->count(),
        );

        return view('admin.users.index',compact('users','count','s'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */

    public function verified(Request $request)
    {
        //
        $s= $request->input('s');

        $users = User::whereActive(1)->oldest()->search($s)->paginate(10);

        return view('admin.users.active',compact('users','s'));
    }

    public function banned(Request $request)
    {
        $s= $request->input('s');
        $users = User::whereBan(1)->oldest()->search($s)->paginate(10);
        return view('admin.users.banned',compact('users','s'));
    }

    public function unverified(Request $request)
    {
        $s= $request->input('s');
        $users = User::whereActive(0)->oldest()->search($s)->paginate(10);
        return view('admin.users.unverified',compact('users','s'));
    }

    public function create()
    {
        //
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request, [

            'name' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'confirm-password' => 'required|same:password'
        ]);

        $settings = Settings::first();

        $user = User::create([

            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'admin'=>$request->admin,
            'active'=>0,
            'membership_id'=>1,
            'membership_started'=>date('Y-m-d'),
            'membership_expired'=>'2020-12-31',
            'token'=>str_random(25),
            'bonus'=> Carbon::now(),

        ]);

        $code = Reflink::latest()->first()->link + 20;

        $reflink = Reflink::create([

            'user_id'   => $user->id,
            'link'      => $code,

        ]);

        $profile = Profile::create([

            'user_id' => $user->id,
            'avatar'=>'uploads/avatars/default.jpg',
            'type'=>$request->type,
            'level'=>1,
            'main_balance'  => $settings->signup_bonus,

        ]);

        Bank::create([

            'user_id'               => $user->id,
            'account_holder'        => '',
            'iban'                  => '',
            'swift'                 => '',
            'bank_name'             => '',
            'bank_address'          => '',
            'bank_phone'            => '',
            'bank_officer_name'     => '',
            'bank_officer_email'    => '',
            
        ]);

        $data = (object) array(

            "email"=>$request->email,
            "password"=>$request->password,
            "token"=>$user->token,
        );

        (new User)->forceFill([
            'email' => $request->email,
        ])->notify(new AdminCreateUser($data));

        session()->flash('message', 'The User Has Been Successfully Created.');
        Session::flash('type', 'success');
        Session::flash('title', 'Created Successful');

        return redirect()->route('admin.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user=User::find($id);
        $invest= Invest::whereUser_id($user->id)->select('amount')->sum('amount');
        $interest= InterestLog::whereUser_id($user->id)->select('amount')->sum('amount');
        $upliner = Referral::whereUser_id($user->id)->count();
        if ($upliner == 1){
            $referral = Referral::whereUser_id($user->id)->first();
            $referrer = $referral->reflink->user;
        }
        $reflink = Reflink::where('user_id',$user->id)->first();
        $totalRefer = Referral::where('reflink_id','=',$reflink->id)->get();
        $ptc= UserLog::whereUser_id($user->id)->whereType(1)->select('amount')->sum('amount');
        $ppv= UserLog::whereUser_id($user->id)->whereType(2)->select('amount')->sum('amount');



        return view('admin.users.show',compact('user','invest','interest','referrer','totalRefer','ptc','ppv','upliner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::find($id);

        return view('admin.users.edit',compact('user'));
    }

    public function unSuspend($id)
    {
        $user=User::find($id);
        $user->ban = 0;
        $user->note = null;
        $user->save();


        session()->flash('message', 'The User Account Has Been Successfully Active.');
        Session::flash('type', 'success');
        Session::flash('title', 'Active Successful');

        return redirect()->back();
    }

    public function suspend(Request $request, $id)
    {
        $this->validate($request, [

            'note'=> 'required|min:10|max:200'

        ]);

        $user=User::find($id);
        $user->ban = 1;
        $user->note = $request->note;
        $user->save();


        session()->flash('message', 'The User Has Been Successfully Suspended.');
        Session::flash('type', 'success');
        Session::flash('title', 'Suspended Successful');

        return redirect()->back();
    }

    public function referral()
    {
        $user = Auth::user();
        $users = User::whereAdmin(0)->oldest()->get();

        $code = Reflink::latest()->first();
        // $code = $user->reflink->link;
        // $link = url('register') . '?ref=' . $code;

        
        $latest_referral = Referral::latest()->first();
        if ($latest_referral == null) {
            $referred_code = $code->link + 20;
        } else {
            $referred_code = $latest_referral->referred_code + 20;
        }
        $referrals = Referral::orderby('user_id')->orderby('referred_code')->get();
        return view('admin.users.referral',compact('users','referrals','referred_code'));
    }

    public function newRefer(Request $request)
    {
        //user;
        $this->validate($request, [

            'referred_code'      => 'required|numeric|unique:referrals',
            'count'     => 'required|numeric',

        ]);

        $reflink = Reflink::where('user_id',$request->user)->first();
        $referrals = Referral::oldest()->get();

        if (count($referrals) == 0) {
            $parent_id = 0;
        } else {
            foreach ($referrals as $referral) {
                if ($reflink->link == $referral->referred_code) {
                    $parent_id = $referral->user_id;
                } elseif (Auth::user()->id == $request->user) {
                    $parent_id = 0;
                } else {
                    $parent_id = Auth::user()->id;
                }
            }
        }

        for ($i=0; $i < $request->count; $i++) {
            $request_code = $request->referred_code + $i;
            if($request_code != Referral::where('referred_code',$request_code)->first())
            {
                $referral = new Referral();
                $referral->user_id = $request->user;
                $referral->referred_code = $request_code;
                $referral->parent_id = $parent_id;
                $referral->status = 0;
                $referral->reflink_id = $reflink->id;
                $referral->save();
            }
        }

        return redirect()->route('admin.user.referShow');
    }

    public function delRefer($id)
    {
        //
        $referral = Referral::findOrFail($id);
        $referral->delete();

        session()->flash('message', 'The Referral Code Has Been Successfully Deleted.');
        Session::flash('type', 'success');
        Session::flash('title', 'Deleted Successful');

        return redirect()->back();
    }

    public function investment($id)
    {
        $user = User::find($id);
        $investments = Interest::whereUser_id($user->id)->latest()->paginate(20);
        return view('admin.users.invest',compact('investments','user'));
    }

    public function interest($id)
    {
        $user = User::find($id);
        $logs = InterestLog::whereUser_id($user->id)->latest()->get();

        return view('admin.users.interest',compact('logs','user'));
    }

    public function details($id)
    {
        $investment = Invest::find($id);
        $interest = Interest::whereInvest_id($investment->id)->first();
        $current = new Carbon($investment->start_time);
        $trialExpires = $current->addDays(30);
        $amount = $investment->amount;
        $percentage =  $investment->plan->percentage;
        $profit = (($percentage / 100) * $amount * $investment->plan->minimum);

        return view('admin.users.preview',compact('investment','trialExpires','interest','profit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User::find($id);

        $this->validate($request, [

            'name'=> 'required',
            'email' => 'required|email',
            'occupation' => 'required|max:50',
            'mobile' => 'required|min:8|max:16',
            'telephone' => 'required|min:8|max:16',
            'address' => 'required|max:80',
            'city' => 'required|max:30',
            'state' => 'required|max:30',
            'postcode' => 'required|max:20',
            'account_holder' => 'required|max:50',
            'iban' => 'required|max:30',
            'swift' => 'required|max:30',
            'bank_name' => 'required|max:50',
            'bank_address' => 'required|max:80',
            'bank_phone' => 'required|min:6|max:20',
        ]);

        if ($user->profile->type == 'Company') {
            $this->validate($request, [

                'bank_officer_name' => 'required|max:50',
                'bank_officer_email' => 'required|email',
            ]);
        }

        if ($request->hasFile('avatar')){

            $this->validate($request, [

                'avatar' => 'required|image|mimes:jpeg,bmp,png,jpg'
            ]);

            $avatar = $request->avatar;

            $avatar_new_name = time().$avatar->getClientOriginalName();

            $avatar->move('uploads/avatars', $avatar_new_name);

            $user->profile->avatar = 'uploads/avatars/'. $avatar_new_name;

            $user->profile->save();

        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->admin = $request->admin;
        $user->active = $request->active;
        $user->profile->type = $request->type;
        $user->profile->main_balance = $request->main_balance;
        $user->profile->referral_balance = $request->referral_balance;
        $user->profile->deposit_balance = $request->deposit_balance;
        $user->profile->occupation = $request->occupation;
        $user->profile->mobile = $request->mobile;
        $user->profile->telephone = $request->telephone;
        $user->profile->address = $request->address;
        $user->profile->address2 = $request->address2;
        $user->profile->city = $request->city;
        $user->profile->state = $request->state;
        $user->profile->postcode = $request->postcode;
        $user->profile->country = $request->country;
        $user->bank->account_holder = $request->account_holder;
        $user->bank->iban = $request->iban;
        $user->bank->swift = $request->swift;
        $user->bank->bank_name = $request->bank_name;
        $user->bank->bank_address = $request->bank_address;
        $user->bank->bank_phone = $request->bank_phone;
        $user->bank->account_holder2 = $request->account_holder2;
        $user->bank->iban2 = $request->iban2;
        $user->bank->swift2 = $request->swift2;
        $user->bank->bank_name2 = $request->bank_name2;
        $user->bank->bank_address2 = $request->bank_address2;
        $user->bank->bank_phone2 = $request->bank_phone2;

        if ($user->profile->type == 'Company') {
            $user->bank->bank_officer_name = $request->bank_officer_name;
            $user->bank->bank_officer_email = $request->bank_officer_email;
            $user->bank->bank_officer_name2 = $request->bank_officer_name2;
            $user->bank->bank_officer_email2 = $request->bank_officer_email2;
        }

        $user->save();

        $user->profile->save();

        $user->bank->save();

        if ($request->has('password')){

            $user->password = bcrypt($request->password);

            $user->save();

        }

        session()->flash('message', 'The User Has Been Successfully Updated.');
        Session::flash('type', 'success');
        Session::flash('title', 'Updated Successful');

        return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::findOrFail($id);
        $user->delete();

        session()->flash('message', 'The User Has Been Successfully Deleted.');
        Session::flash('type', 'success');
        Session::flash('title', 'Deleted Successful');

        return redirect()->back();
    }




    ///
    public function cashLinks($id)
    {
        $user = User::find($id);
        $logs = Advert::whereUser_id($user->id)->whereStatus(1)->latest()->get();
        return view('admin.users.ptc',compact('logs','user'));
    }

    public function cashVideos($id)
    {
        $user = User::find($id);
        $logs = Video::whereUser_id($user->id)->whereStatus(1)->latest()->get();
        return view('admin.users.ppv',compact('logs','user'));
    }

    public function LinkShare($id)
    {
        $user = User::find($id);
        $logs = Share::whereUser_id($user->id)->whereStatus(1)->latest()->get();
        return view('admin.users.share',compact('logs','user'));
    }

    public function transfer($id)
    {
        $user = User::find($id);
        $logs = TransferLog::whereUser_id($user->id)->whereStatus(1)->latest()->get();
        return view('admin.users.transfer',compact('logs','user'));
    }

    public function deposit($id)
    {
        $user = User::find($id);
        $logs = Deposit::whereUser_id($user->id)->whereStatus(1)->latest()->get();
        return view('admin.users.deposit',compact('logs','user'));
    }

    public function withdraw($id)
    {
        $user = User::find($id);
        $logs = Withdraw::whereUser_id($user->id)->whereStatus(1)->latest()->get();
        return view('admin.users.withdraw',compact('logs','user'));
    }

    public function admin($id)
    {
        $user = User::find($id);

        $user->admin=1;

        $user->save();

        session()->flash('message', 'The User Has Been Successfully Get Admin Permission.');
        Session::flash('type', 'success');
        Session::flash('title', 'Permission Granted');

        return redirect()->back();
    }

    public function adminRemove($id)
    {
        $user = User::find($id);

        $user->admin=0;

        $user->save();

        session()->flash('message', 'The User Has Been Successfully Removed Admin Permission.');
        Session::flash('type', 'success');
        Session::flash('title', 'Permission Removed');

        return redirect()->back();
    }

}
