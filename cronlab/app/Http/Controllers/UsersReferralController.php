<?php

namespace App\Http\Controllers;

use App\Referral;
use App\ReferralLink;
use App\ReferralRelationship;
use App\Reflink;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersReferralController extends Controller
{
    //
    public function __construct()
    {

        $this->middleware('auth');

    }

    public function index()
    {
        $user = Auth::user();

        $code = $user->reflink->link;
        
        $parent = Referral::whereReferred_code($code)->first();

        if ($parent != null)
        {
            $parent_id = $parent->user_id;
            $parent_info = User::find($parent->user_id);
        } else {
            $parent_id = 0;
        }

        $referrals = Referral::whereUser_id($user->id)->get();
        
        $referred_code = [];
        foreach ($referrals as $referral) {
            array_push($referred_code, $referral->referred_code);
        }

        $num = count($referred_code);

        $reflinks = Reflink::oldest()->paginate(10);

        return view('user.myreferral',compact('user','parent_id','parent_info','reflinks','referred_code','num'));
    }

}
